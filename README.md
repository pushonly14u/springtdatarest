To run the application use following command

###mvn spring-boot:run

As it used h2 database(In memory) so no need to connect with any external db.
If you want then you can change application.properties file.

Predefined data sore in data.sql file

Once application started go to

localhost:8080/api

and there you find the exposed API
you are allow to call GET,POST,PUT and DELETE API
We also have custom api like find by name.
As we don't want ot expose GetAll data api, so we restrict it. 