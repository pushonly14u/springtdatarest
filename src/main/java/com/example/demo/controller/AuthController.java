package com.example.demo.controller;

import com.example.demo.model.AuthLogin;
import com.example.demo.model.AuthUser;
import com.example.demo.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @PostMapping("/token")
    public ResponseEntity<String> getToken(@RequestBody AuthLogin auth){
        return authService.getToken(auth);
    }

    @PostMapping("/signup")
    public ResponseEntity<String> createUser(@RequestBody AuthUser user) {
        return authService.createUser(user);
    }
}