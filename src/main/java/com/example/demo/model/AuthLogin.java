package com.example.demo.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AuthLogin {
    @NotNull
    private String username;
    @NotNull
    private String password;
}
