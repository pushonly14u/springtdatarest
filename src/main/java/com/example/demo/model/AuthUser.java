package com.example.demo.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AuthUser {
    @NotNull
    private String name;
    @NotNull
    private String email;
    @NotNull
    private String company;
    @NotNull
    private String password;
}
