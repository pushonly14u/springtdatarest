package com.example.demo.repository;

import com.example.demo.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import javax.annotation.security.RolesAllowed;
import static com.example.demo.utils.Constants.ROLE_ADMIN;

@RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer> {

    @Override
    @RolesAllowed(ROLE_ADMIN)
    Iterable<Customer> findAll(Sort var1);

    @Override
    @RolesAllowed(ROLE_ADMIN)
    Page<Customer> findAll(Pageable var1);

    Customer findByName(@Param("name") String name);
}
