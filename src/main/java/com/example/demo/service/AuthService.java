package com.example.demo.service;

import com.example.demo.model.AuthLogin;
import com.example.demo.model.AuthUser;
import org.springframework.http.ResponseEntity;

public interface AuthService {
    ResponseEntity<String> getToken(AuthLogin auth);
    ResponseEntity<String> createUser(AuthUser user);
}
