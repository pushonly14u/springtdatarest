package com.example.demo.service.impl;

import com.example.demo.model.AuthLogin;
import com.example.demo.model.AuthUser;
import com.example.demo.service.AuthService;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    RestTemplate restTemplate;

    @Value(value = "${auth0.issuer}")
    private String baseUrl;
    @Value(value = "${auth0.apiAudience}")
    private String audience;
    @Value(value = "${auth0.client_id}")
    private String client_id;
    @Value(value = "${auth0.client_secret}")
    private String client_secret;
    @Value(value = "${auth0.connection}")
    private String connection;


    @Override
    public ResponseEntity<String> getToken(AuthLogin auth) {

        JSONObject jsonBody = new JSONObject();
        jsonBody.put("grant_type", "password");
        jsonBody.put("username", auth.getUsername());
        jsonBody.put("password", auth.getPassword());
        jsonBody.put("audience", audience);
        jsonBody.put("client_id", client_id);
        jsonBody.put("client_secret", client_secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(jsonBody.toString(), headers);

        return restTemplate.postForEntity(baseUrl+"oauth/token", request, String.class);
    }

    @Override
    public ResponseEntity<String> createUser(AuthUser user) {

        JSONObject jsonBody = new JSONObject();
        JSONObject metadata = new JSONObject();
        metadata.put("company", user.getCompany());
        jsonBody.put("name",user.getName());
        jsonBody.put("client_id",client_id);
        jsonBody.put("email", user.getEmail());
        jsonBody.put("password", user.getPassword());
        jsonBody.put("connection", connection);
        jsonBody.put("user_metadata",metadata);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(jsonBody.toString(), headers);

        return restTemplate.postForEntity(baseUrl+"dbconnections/signup", request, String.class);
    }
}
