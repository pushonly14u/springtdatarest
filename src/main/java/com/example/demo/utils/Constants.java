package com.example.demo.utils;

public class Constants {
    public static final String ROLE_AIRLINE = "AIRLINE";
    public static final String ROLE_INSURANCE = "INSURANCE";
    public static final String ROLE_FLOWN = "FLOWN";
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_USER = "USER";
}